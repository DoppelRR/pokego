﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;

public class Playercontroller : MonoBehaviour {

    //Server Settings
    private TcpClient client;
    private NetworkStream stream;
    private Dictionary<int, GameObject> players = new Dictionary<int, GameObject>();

    //General Settings
    public float movementSpeed;
	public float mouseSensitivity;
	public float upDownRange;
	float pitch = 0;
	float yaw = 0;

	//Player info
	private CharacterController cc;
	public Vector3 playerPos;

	// Use this for initialization
	void Start () {

        Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		
		cc = GetComponent<CharacterController> ();
		
		transform.position = new Vector3 (playerPos.x * 2, 2, playerPos.z * 2);

        //Server stuff
        try {
            // Create a TcpClient.
            // Note, for this client to work you need to have a TcpServer 
            // connected to the same address as specified by the server, port
            // combination.
            int port = 26656;
            client = new TcpClient("127.0.0.1", port);
            stream = client.GetStream();

            //send own data for first time
            sendToServer(Data.getType() + ":0.0:2.0:0.0");
        } catch (System.ArgumentNullException e) {
            Debug.Log("ArgumentNullException: {0}" + e);
        } catch (SocketException e) {
            Debug.Log("SocketException: {0}" + e);
        }
    }

	// Update is called once per frame
	void Update () {

		if (cc == null)
			return;

		//Rotation
		yaw -= Input.GetAxis ("Mouse X") * mouseSensitivity;
		pitch -= Input.GetAxis ("Mouse Y") * mouseSensitivity;
		pitch = Mathf.Clamp(pitch, -upDownRange, upDownRange);
		transform.localRotation = Quaternion.Euler(pitch, -yaw, 0);

		//Movement
		float forwardSpeed = Input.GetAxis ("Vertical") * movementSpeed;
		float sideSpeed = Input.GetAxis ("Horizontal") * movementSpeed;

		Vector3 speed = new Vector3 (sideSpeed, 0, forwardSpeed);

		speed = transform.rotation * speed;
		speed.Set (speed.x, 0, speed.z);

		cc.Move (speed * Time.deltaTime);

        //server stuff
        //send Data
        sendToServer("send:" + transform.position.x + ":" + transform.position.y + ":" + transform.position.z);

        //receive data
        string rawServerData = receive();
        if (rawServerData.Equals(" |"))
            return;

        Debug.Log(rawServerData);
        foreach (string user in rawServerData.Split('|')) {
            if (user.Equals(" "))
                continue;

            string[] userData = user.Split(':');
            int id = int.Parse(userData[0]);

            if (players.ContainsKey(id)) {
                float x = float.Parse(userData[2]);
                float y = float.Parse(userData[3]);
                float z = float.Parse(userData[4]);
                ((Rigidbody)players[id].GetComponent("Rigidbody")).position = new Vector3(x, y, z);
            } else {
                float x = float.Parse(userData[2]);
                float y = float.Parse(userData[3]);
                float z = float.Parse(userData[4]);
                GameObject player = null;
                if (Data.getType().Equals("sphere")) {
                    player = GameObject.CreatePrimitive(PrimitiveType.Cube);
                } else if (Data.getType().Equals("cube")) {
                    player = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                }
                player.transform.position = new Vector3(x, y, z);
                Rigidbody rb = player.AddComponent<Rigidbody>();
                rb.isKinematic = true;
                players[id] = player;
            }
        }
    }

    public void sendToServer(string message) {
        byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
        stream.Write(data, 0, data.Length);
    }

    string receive() {
        sendToServer("receive");

        // Receive the TcpServer.response.

        // Buffer to store the response bytes.
        byte[] data = new byte[256];

        // String to store the response ASCII representation.
        string dataString = string.Empty;

        // Read the first batch of the TcpServer response bytes.
        int bytes = stream.Read(data, 0, data.Length);
        dataString = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

        return dataString;
    }
}
