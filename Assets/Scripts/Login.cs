﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Login : MonoBehaviour {

	public Button but;
	public Dropdown drop;

	// Use this for initialization
	void Start () {
		but.onClick.AddListener (() => {
			Application.LoadLevel("Arena");
            if (drop.value == 0) {
                Data.setType("sphere");
            }
            else if (drop.value == 1) {
                Data.setType("cube");
            }
		});
	}
}
