﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;

public static class Data {

	private static string type;

	public static void setType(string newType) {
		type = newType;
	}

	public static string getType() {
		return type;
	}
}
